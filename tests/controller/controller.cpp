#include <catch2/catch.hpp>

#include <umrob/robot_controller.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1" />
    <link name="b2" />
    <link name="tcp" />
    <joint name="j1" type="revolute">
        <parent link="root"/>
        <child link="b1"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j2" type="revolute">
        <parent link="b1"/>
        <child link="b2"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j3" type="fixed">
        <parent link="b2"/>
        <child link="tcp"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
</robot>
)";

TEST_CASE("Controller") {
    constexpr double time_step = 0.01;
    auto model = umrob::RobotModel(planar_robot);
    auto robot = umrob::Robot(model);
    auto controller = umrob::RobotController(robot, time_step);

    model.jointPosition("j1") = M_PI / 2.;
    model.jointPosition("j2") = M_PI / 2.;

    robot.setStateFromModel();

    auto& cp = robot.controlPoint("tcp");

    auto run_controller = [&controller]() {
        bool ok{false};
        controller.reset();
        controller.robot().update();
        REQUIRE_NOTHROW(ok = controller.update());
        REQUIRE(ok);
    };

    auto is_approx = [](auto value, auto ref, decltype(value) epsilon = 1e-9) {
        return std::abs(value - ref) < epsilon;
    };

    SECTION("No task and no constraints") {
        REQUIRE_THROWS(controller.update());
    }

    SECTION("Task creation") {
        constexpr double weight{1.};
        constexpr double gain{1.};
        REQUIRE_NOTHROW(controller.addControlPointVelocityTask("tcp", weight));
        REQUIRE_NOTHROW(
            controller.addControlPointPositionTask("tcp", weight, gain));
    }

    SECTION("Unconstrained velocity task") {
        constexpr double weight{1.};
        controller.addControlPointVelocityTask("tcp", weight);

        run_controller();

        REQUIRE(robot.joints().command.velocity.isZero());

        cp.target.velocity.x() = 0.1;
        run_controller();

        REQUIRE(not robot.joints().command.velocity.isZero());
        REQUIRE(cp.command.velocity.isApprox(cp.target.velocity));

        cp.target.velocity.x() = 100;
        run_controller();

        REQUIRE(cp.command.velocity.isApprox(cp.target.velocity));
    }

    SECTION("Velocity task with joint velocity constraint") {
        constexpr double weight{1.};
        controller.addControlPointVelocityTask("tcp", weight);

        robot.joints().limits.max_velocity.setOnes();
        controller.addJointsVelocityConstraint();

        run_controller();

        REQUIRE(robot.joints().command.velocity.isZero());

        cp.target.velocity.x() = 0.1;
        run_controller();

        REQUIRE(not robot.joints().command.velocity.isZero());
        REQUIRE(cp.command.velocity.isApprox(cp.target.velocity, 1e-6));

        cp.target.velocity.x() = 100;
        run_controller();

        REQUIRE(not cp.command.velocity.isApprox(cp.target.velocity, 1e-6));
        REQUIRE(robot.joints().command.velocity.cwiseAbs().isApprox(
            robot.joints().limits.max_velocity));
    }

    SECTION("Velocity task with joint position constraint") {
        constexpr double weight{1.};
        controller.addControlPointVelocityTask("tcp", weight);

        robot.joints().limits.max_position.setConstant(M_PI);
        robot.joints().limits.min_position.setZero();
        controller.addJointsPositionConstraint();

        run_controller();

        REQUIRE(robot.joints().command.velocity.isZero());

        cp.target.velocity.x() = 0.01;
        run_controller();

        REQUIRE(not robot.joints().command.velocity.isZero());
        REQUIRE(cp.command.velocity.isApprox(cp.target.velocity, 1e-6));
        REQUIRE((robot.joints().limits.max_position -
                 robot.joints().command.position)
                    .minCoeff() >= 0);
        REQUIRE((robot.joints().command.position -
                 robot.joints().limits.min_position)
                    .minCoeff() >= 0);

        cp.target.velocity.x() = 100;
        run_controller();

        REQUIRE(not cp.command.velocity.isApprox(cp.target.velocity, 1e-6));
        REQUIRE((robot.joints().limits.max_position -
                 robot.joints().command.position)
                    .minCoeff() >= 0);
        REQUIRE((robot.joints().command.position -
                 robot.joints().limits.min_position)
                    .minCoeff() >= 0);
        for (Eigen::Index i = 0; i < robot.joints().command.position.size();
             i++) {
            auto p = robot.joints().command.position[i];
            auto min = robot.joints().limits.min_position[i];
            auto max = robot.joints().limits.max_position[i];
            REQUIRE((is_approx(p, min) or is_approx(p, max)));
        }
    }

    SECTION("Velocity task with task velocity constraint") {
        constexpr double weight{1.};
        controller.addControlPointVelocityTask("tcp", weight);

        cp.limits.max_velocity.setOnes();
        controller.addControlPointVelocityConstraint("tcp");

        run_controller();

        REQUIRE(robot.joints().command.velocity.isZero());

        cp.target.velocity.x() = 0.1;
        run_controller();

        REQUIRE(not robot.joints().command.velocity.isZero());
        REQUIRE(cp.command.velocity.isApprox(cp.target.velocity, 1e-6));

        cp.target.velocity.x() = 2;
        run_controller();

        REQUIRE(not cp.command.velocity.isApprox(cp.target.velocity, 1e-6));
        REQUIRE(is_approx(cp.command.velocity.x(), cp.limits.max_velocity.x()));
    }

    SECTION("Position task") {
        constexpr double weight{1.};
        constexpr double gain{1};
        constexpr size_t max_steps{100};
        constexpr double threshold{0.001};

        model.jointPosition("j1") = M_PI / 4.;
        model.jointPosition("j2") = M_PI / 4.;
        model.update();
        cp.target.position = model.linkPose("tcp");
        robot.updatePositionVectors();

        model.jointPosition("j1") = M_PI / 2.;
        model.jointPosition("j2") = M_PI / 2.;
        model.update();
        cp.state.position = model.linkPose("tcp");

        controller.addControlPointPositionTask("tcp", weight, gain);
        controller.addJointsVelocityConstraint();
        robot.updatePositionVectors();

        robot.setStateFromModel();

        // Intentional use of auto here to refer to the expression and not its
        // current value
        auto error = cp.target.position_vector - cp.state.position_vector;
        auto previous_error = error.eval();

        size_t step{0};
        for (; step < max_steps; step++) {
            run_controller();
            robot.joints().state = robot.joints().command;
            cp.state = cp.command;

            robot.updateModelFromState();

            REQUIRE(error.norm() < previous_error.norm());

            if (error.norm() < threshold) {
                break;
            }

            previous_error = error;
        }
        REQUIRE(step < max_steps);
    }
}